# oh_my_zsh_install

#### 介绍
oh_my_zsh 国内安装修改镜像 直连gitee官方源

#### 安装教程

##### Install oh-my-zsh via curl
```shell
sh -c "$(curl -fsSL https://gitee.com/Devkings/oh_my_zsh_install/raw/master/install.sh)"
```
##### Install oh-my-zsh via wget
```shell
sh -c "$(wget https://gitee.com/Devkings/oh_my_zsh_install/raw/master/install.sh -O -)"
```


